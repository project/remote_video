<?php

namespace Drupal\remote_video\Services;

use Drupal\Core\Config\ConfigFactoryInterface;

class RemoteVideoConfigService implements RemoteVideoConfigServiceInterface {
    private const CONFIG_NAME = 'remote_video.settings';

    private ConfigFactoryInterface $configFactory;

    public function __construct(ConfigFactoryInterface $configFactory) {
        $this->configFactory = $configFactory;
    }

    public function get($key)
    {
        return $this->configFactory->getEditable(static::CONFIG_NAME)->get($key) ?? NULL;
    }

    public function set($key, $value): void
    {
        $this->configFactory->getEditable(static::CONFIG_NAME)
            ->set($key, $value)
            ->save();
    }

    public function clear($key): void
    {
        $this->configFactory->getEditable(static::CONFIG_NAME)->clear($key)->save();
    }

    public function isRemoteVideoServicesFound(): bool
    {
        return !empty($this->get('services'));
    }

    public function getEditableConfigNames(): array
    {
        $names[] = "remote_video.settings:services";
        return $names;
    }

    public function getRemoteVideoServiceIds(): array
    {
        return $this->get('services') ?? [];
    }

    public function getRemoteVideoServices(): RemoteVideoServiceCollection
    {
        $serviceIds = array_values($this->getRemoteVideoServiceIds());
        return RemoteVideoServiceCollection::load($serviceIds);
    }

    public function getRemoteVideoService(string $remoteVideoServiceId): ?RemoteVideoServiceInterface
    {
        return $this->getRemoteVideoServices()->get($remoteVideoServiceId);
    }

    public function getRemoteVideoServiceFromUrl(string $url): ?RemoteVideoServiceInterface
    {
        foreach ($this->getRemoteVideoServices() as $service) {
            /** @var RemoteVideoServiceInterface $service */
            if ($service->isUrlSupported($url)) {
                return $service;
            }
        }
        return NULL;
    }

    public function registerRemoteVideoService(string $remoteVideoServiceId): void
    {
        $services = $this->get('services') ?? [];
        $services[$remoteVideoServiceId] = 'remote_video_' . $remoteVideoServiceId;
        $this->set('services', $services);
    }

    public function removeRemoteVideoService(string $remoteVideoServiceId): void
    {
        $services = $this->get('services');
        unset($services[$remoteVideoServiceId]);
        $this->set('services', $services);
        $this->clear($remoteVideoServiceId);
    }
}
