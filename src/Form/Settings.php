<?php

namespace Drupal\remote_video\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\remote_video\Services\RemoteVideoConfigServiceInterface;
use Drupal\remote_video\Services\RemoteVideoServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Settings extends ConfigFormBase {

    private RemoteVideoConfigServiceInterface $remoteVideoConfigService;

    public function __construct(RemoteVideoConfigServiceInterface $configService)
    {
        $this->remoteVideoConfigService = $configService;
    }

    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('remote_video.config'),
        );
    }

    public function getFormId() {
        return 'remote_video_settings';
    }

    protected function getEditableConfigNames() {
        return $this->remoteVideoConfigService->getEditableConfigNames();
    }

    public function buildForm(array $form, FormStateInterface $form_state) {
        $form['#tree'] = true;
        $remoteVideoServices = $this->remoteVideoConfigService->getRemoteVideoServices();
        if (!$remoteVideoServices->count()) {
            $markup = "No remote video services are available. Please install a module that provides a remote video service: <ul>";
            foreach ($this->getCompatibleServiceModules() as $compatibleServiceModule) {
                $markup .= "<li><a href='https://www.drupal.org/progject/{$compatibleServiceModule}' target='_blank'>{$compatibleServiceModule}</a></li>";
            }
            $markup .= "</ul>";
            $form['no_services'] = [
                '#markup' => $markup,
            ];
            return $form;
        }

        foreach ($remoteVideoServices as $remoteVideoService) {
            /** @var RemoteVideoServiceInterface $remoteVideoService */
            $remoteVideoServiceId = $remoteVideoService->id();
            $form[$remoteVideoServiceId] = [
                '#type' => 'fieldset',
                '#title' => $remoteVideoService->getTitle(),
            ];
            foreach ($remoteVideoService->getEditableConfigKeys() as $key) {
                $type = $remoteVideoService->getConfigFieldType($key);
                $value = $remoteVideoService->getConfig($key);
                $description = $remoteVideoService->getConfigFieldDescription($key);
                if ($type === 'password' && !empty($value)) {
                    $description = "<strong>Current value is hidden.</strong> {$description}";
                }
                $form[$remoteVideoServiceId][$key] = [
                    '#type' => $type,
                    '#title' => $remoteVideoService->getConfigFieldTitle($key),
                    '#description' => $description,
                    '#default_value' => $value,
                ];
            }
        }

        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => "Save",
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        foreach ($this->remoteVideoConfigService->getRemoteVideoServices() as $remoteVideoService) {
            /** @var RemoteVideoServiceInterface $remoteVideoService */
            $id = $remoteVideoService->id();
            foreach ($remoteVideoService->getEditableConfigKeys() as $key) {
                $value = $form_state->getValue($id)[$key];
                $validationPattern = $remoteVideoService->getEditableConfigValidationPattern($key);
                if ($validationPattern && 1 !== preg_match($validationPattern, $value)) {
                    $form_state->setErrorByName($id . "[$key]", "{$remoteVideoService->getConfigFieldTitle($key)} is invalid. Required pattern: {$validationPattern}");
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        foreach ($this->remoteVideoConfigService->getRemoteVideoServices() as $remoteVideoService) {
            /** @var RemoteVideoServiceInterface $remoteVideoService */
            $remoteVideoServiceId = $remoteVideoService->id();
            foreach ($remoteVideoService->getEditableConfigKeys() as $key) {
                $remoteVideoService->setConfig($key, $form_state->getValue($remoteVideoServiceId)[$key]);
            }
        }
        parent::submitForm($form, $form_state);
    }

    private function getCompatibleServiceModules(): array
    {
        return [
            'remote_video_youtube',
            'remote_video_vimeo',
        ];
    }

}
