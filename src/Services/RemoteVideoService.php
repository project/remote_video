<?php

namespace Drupal\remote_video\Services;

use Drupal\Core\StringTranslation\TranslatableMarkup;

abstract class RemoteVideoService implements RemoteVideoServiceInterface {
    protected string $id;
    protected string $title;
    protected string $domain;

    private array $editableConfigKeys = [];

    private array $editableConfigKeyTitles = [];

    private array $editableConfigKeyDescriptions = [];

    private array $editableConfigKeyValidationPatterns = [];

    private RemoteVideoConfigServiceInterface $remoteVideoConfig;

    public function __construct(RemoteVideoConfigServiceInterface $remoteVideoConfig) {
        $this->remoteVideoConfig = $remoteVideoConfig;
        $this->setEditableConfigKey('api_key');
        $this->setEditableConfigFieldTitle('api_key', new TranslatableMarkup('API Key'));
    }

    public function setConfig(string $key, array|string $value): void
    {
        $config = $this->remoteVideoConfig->get($this->id);
        $config[$key] = $value;
        $this->remoteVideoConfig->set($this->id, $config);
    }

    public function getConfig($key): array|string|null
    {
        return $this->remoteVideoConfig->get($this->id)[$key] ?? NULL;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function getTitle(): TranslatableMarkup
    {
        return new TranslatableMarkup($this->title);
    }

    public function isUrlSupported(string $url): bool
    {
        return str_contains($url, $this->domain);
    }

    public function isConfigured(): bool
    {
        return $this->isApiKeyFound();
    }

    public function isApiKeyFound(): bool
    {
        return $this->getApiKey() !== NULL;
    }

    public function getConfigFieldType(string $key): string
    {
        $protected = ['password', 'key', 'token', 'secret'];
        foreach ($protected as $word) {
            if (str_contains($key, $word)) {
                return 'password';
            }
        }
        return 'textfield';
    }

    public function getEditableConfigKeys(): array
    {
        return $this->editableConfigKeys;
    }

    public function setEditableConfigKey(string $key)
    {
        $this->editableConfigKeys[] = $key;
    }

    public function setEditableConfigFieldTitle(string $key, TranslatableMarkup $param)
    {
        $this->editableConfigKeyTitles[$key] = $param;
    }

    public function getConfigFieldTitle(string $key): ?string
    {
        return $this->editableConfigKeyTitles[$key] ?? NULL;
    }

    public function setEditableConfigFieldDescription(string $key, TranslatableMarkup $param)
    {
        $this->editableConfigKeyDescriptions[$key] = $param;
    }

    public function getConfigFieldDescription(string $key): ?string
    {
        return $this->editableConfigKeyDescriptions[$key] ?? NULL;
    }

    public function setEditableConfigValidationPattern(string $key, string $pattern): void
    {
        $this->editableConfigKeyValidationPatterns[$key] = $pattern;
    }

    public function getEditableConfigValidationPattern(string $key): ?string
    {
        return $this->editableConfigKeyValidationPatterns[$key] ?? NULL;
    }

}
