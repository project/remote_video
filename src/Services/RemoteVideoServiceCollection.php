<?php

namespace Drupal\remote_video\Services;

class RemoteVideoServiceCollection extends \ArrayObject
{
    public static function load(array $remoteVideoServiceIds): RemoteVideoServiceCollection
    {
        $collection = new static();
        foreach ($remoteVideoServiceIds as $remoteVideoServiceId) {
            $collection[] = \Drupal::service($remoteVideoServiceId);
        }
        return $collection;
    }

    public function get(string $remoteVideoServiceId): ?RemoteVideoServiceInterface
    {
        foreach ($this as $service) {
            if ($service->id() === strtolower($remoteVideoServiceId)) {
                return $service;
            }
        }
        return NULL;
    }

}
