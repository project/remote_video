<?php

namespace Drupal\remote_video\Services;

use Drupal\Core\StringTranslation\TranslatableMarkup;

interface RemoteVideoServiceInterface {

    public function getTitle(): TranslatableMarkup;

    public function getEditableConfigKeys(): array;

    public function isUrlSupported(string $url): bool;

    public function isConfigured(): bool;

    public function isApiKeyFound(): bool;

    public function getApiKey(): ?string;

    public function id();

    public function getConfigFieldType(string $key): string;

    public function getConfigFieldTitle(string $key): ?string;

    public function getConfigFieldDescription(string $key): ?string;

    public function setEditableConfigValidationPattern(string $key, string $pattern): void;

    public function getEditableConfigValidationPattern(string $key): ?string;

    public function getVideoInfo(string $url): VideoInfoResponseInterface;

}
