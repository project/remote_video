<?php

namespace Drupal\remote_video\Services;

interface VideoInfoResponseInterface {

    public function getTitle(): string;

    public function getChannelTitle(): string;

    public function getPublishedDateTime(): \DateTime;

    public function getDefaultAudioLanguage(): string;

    public function getDescription(): string;

    public function getDuration(): int;

    public function getThumbnailSourceUrl(): string;

    public function getId(): string;

    public function getNewThumbnailFilename(): string;
}
