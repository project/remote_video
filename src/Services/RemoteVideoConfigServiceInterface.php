<?php

namespace Drupal\remote_video\Services;

interface RemoteVideoConfigServiceInterface {
    public function get($key);

    public function set($key, $value): void;

    public function clear($key): void;

    public function isRemoteVideoServicesFound(): bool;

    public function getRemoteVideoServiceIds(): array;

    public function getRemoteVideoServices(): RemoteVideoServiceCollection;

    public function getRemoteVideoService(string $remoteVideoServiceId): ?RemoteVideoServiceInterface;

    public function getRemoteVideoServiceFromUrl(string $url): ?RemoteVideoServiceInterface;

    public function registerRemoteVideoService(string $remoteVideoServiceId): void;

    public function removeRemoteVideoService(string $remoteVideoServiceId): void;

    public function getEditableConfigNames(): array;

}
