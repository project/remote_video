# Remote Video
This module expands core `remote_video` functionality so more Remote Video metadata can be stored in Drupal. This allows for more
advanced video management and display options.

## Installation
1. Install the module as you would normally install a contributed Drupal module. Visit [Installing contributed modules](https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8) for further information.
2. Enable the module by navigating to `Extend` and selecting the `Remote Video` module.
3. Configure the module by navigating to `/admin/config/services/remote_video`.
4. If no related `remote_video_*` modules are enabled, the module will suggest a few to consider.
5. Once you have installed & enabled one or more of those modules, configuration options will appear upon page refresh.

## Usage
For this example, use of YouTube is required.

1. Install and enable the `remote_video_youtube` module.
2. Navigate to `/media/add/remote_video`.
3. Set `Video URL` to [Driesnote: Lille DrupalCon 2023](https://www.youtube.com/watch?v=tFxlDBLkLJc).
4. Save the form. You will see the video's metadata has been imported. Customize it, if necessary & Publish.

## API
This module may be configured by other `remote_video_*` modules (e.g. `remote_video_youtube`).

Refer to [remote_video_youtube](https://www.drupal.org/project/remote_video_youtube) for an example. Clone it if you would like to start making a new `remote_video_*` module.

After installation & registration, the module provides a `remote_video_youtube` service that can be used
to fetch video metadata. The `remote_video_youtube` service relies on config managed by this module to make
API requests.
